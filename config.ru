# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
#
require 'date'
require 'rack'
#
timeStart = DateTime.now.strftime('%Q').to_i
T5_ENGINE = ENV['t5Engine'].strip
T5_CORE = ENV['t5Core'].split(' ')
T5_MODULES = ENV['t5Modules'].split(' ')
#
t5CoreFullname = ''
T5_CORE.each { |core| t5CoreFullname = t5CoreFullname + "core_#{core} " }
#
BUNDLE_PARSERS = t5CoreFullname + ENV['t5Modules']
BUNDLE_WITH = BUNDLE_PARSERS.split(' ').join(':')
#
require 'rubygems'
Bundler.require("engine_#{T5_ENGINE}")
require T5_ENGINE

require 'bundler'
require 'bundler/setup'
require 'bundler/cli'
require 'bundler/cli/info'
#
Bundler.settings.set_command_option :parsers, BUNDLE_PARSERS
Bundler.settings.set_command_option :with, BUNDLE_WITH
Bundler.settings.set_command_option :auto_install, true
Bundler.require(:default)
#
require 'openssl'
#OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE  if ENV.key?('t5SslVerify') && ENV['t5SslVerify'] == 'none'к
#
require './lib/loaders/core'
require './lib/sys/grape/basic' if T5_CORE.include?('rest')

###################
# 1
require './lib/loaders/initial'
# 2
if T5_CORE.include?('rest')
  Grape.configure do |config|
    config.param_builder = Grape::Extensions::Ok::ParamBuilder
  end
end
# 3
BUNDLE_PARSERS.split(' ').each do |group|
  Bundler.require(group.to_sym)
end
# 4
Dir.glob(File.join("./lib/sys/**", "*.rb")).each {|f| require f}
# 5
generateReModules 
# 6
Dir.glob(File.join("./lib/re/**", "*.rb")).each {|f| require f}
# 7
generateContainersModules 
# 8
includeT5
# 9
require './lib/loaders/grapeApiConstructor' if T5_CORE.include?('rest')
# 10
Memex::Reflection::API::Base::Root.compile! if T5_CORE.include?('rest')
# 11
#t5::API::Linter.stdout(linter: t5::API::Linter.description(class: t5::Env::Base.api[:class]))
# 12
GC::Profiler.enable
# 13
ElasticAPM::Grape.start(t5::API, t5::Env.apm) if T5_MODULES.include?('dev_apm')
# 14
if T5_CORE.include?('rest')
  run Memex::Reflection::API::Base::Root
else
  run Config.runner
end
timeEnd = DateTime.now.strftime('%Q').to_i
pp "Started in #{((timeEnd.to_f - timeStart.to_f)/1000).round(5)} seconds"
###################




#################  DEV  ####################
# use Rack::Cors do
#   allow do
#     origins '*'
#     resource '*', headers: :any, methods: %i[get post put patch delete options]
#   end
# end


# use Rack::Static,
#   :urls => ["/images", "/lib", "/js", "/css"],
#   :root => "public/swagger_ui"
# 
# map '/swagger-ui' do
#   run lambda { |env|
#     [
#       200,
#       {
#         'Content-Type'  => 'txt-html',
#         'Cache-Control' => 'public, max-age=86400'
#       },
#       File.open('public/swagger_ui/index.html', File::RDONLY)
#     ]
#   }
# end


#require 'rack/unreloader'
#Unreloader = Rack::Unreloader.new{Api}
#require 'grape'
#Unreloader.require './api/_start'
#run Unreloader

#################  DEV  ####################
