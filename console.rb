#!/usr/bin/ruby.ruby3.1
# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
#
require 'date'
timeStart = DateTime.now.strftime('%Q').to_i
###################
context = ARGV[0]
method  = ARGV[1]
params  = ARGV[2]
conArray = context.split('::')
ENV['memexContainer'] = conArray[0]
###################
File.open("../_run/containers/env/_global.sh").each do |line|
  c = line.strip.gsub('\'', '').gsub('\"', '').split('=')
  ENV[c[0]] = c[1] if c[1]
end
File.open("../_run/containers/env/#{conArray[0].downcase}.sh").each do |line|
  c = line.strip.gsub('\'', '').gsub('\"', '').split('=')
  ENV[c[0]] = c[1] if c[1]
end
###################
T5_CORE = ENV['t5Core'].split(' ')
t5CoreFullname = ''
T5_CORE.each { |core| t5CoreFullname = t5CoreFullname + "core_#{core} " }
BUNDLE_PARSERS = t5CoreFullname + ENV['t5Modules']
BUNDLE_WITH = BUNDLE_PARSERS.split(' ').join(':')
#
require 'rubygems'

require 'bundler'
require 'bundler/setup'
require 'bundler/cli'
require 'bundler/cli/info'

Bundler.settings.set_command_option :parsers, BUNDLE_PARSERS
Bundler.settings.set_command_option :with, BUNDLE_WITH
Bundler.settings.set_command_option :auto_install, true
Bundler.require(:default)
#
require 'openssl'
#OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE  if ENV.key?('t5SslVerify') && ENV['t5SslVerify'] == 'none'
#
require './lib/loaders/core'
require './lib/sys/grape/basic' if T5_CORE.include?('rest')
# 1
require './lib/loaders/initial'
# 2
BUNDLE_PARSERS.split(' ').each do |group|
  Bundler.require(group.to_sym)
end
#
Dir.glob(File.join("./lib/sys/**", "*.rb")).each {|f| require f}
# 3
Dir.glob(File.join("./lib/re/**", "*.rb")).each {|f| require f}
# 4
includeT5
#
Dir.glob(File.join("../_run/containers/runtime/#{conArray[0].downcase}/*/**", "*.rb")).each {|f| require f}
Dir.glob(File.join("../_run/containers/runtime/#{conArray[0].downcase}/", "env.rb")).each {|f| p f; require f}
Dir.glob(File.join("../_run/containers/runtime/#{conArray[0].downcase}/", "config.rb")).each {|f| p f; require f}

# 7
GC::Profiler.enable
####

res = Object.const_get("Memex::Containers::#{context}").send(method.to_sym, O.k(Oj.load(params)))
p res
