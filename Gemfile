# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

source 'https://rubygems.org'

############ // MANDATORY \\ ############
## Base
ruby '>= 3.1'
gem 'rake', ">= 13"
gem 'rack', ">= 3.0"

#gem 'lmdb', '>= 0.5'

## HTTP/REST
gem 'faraday'#, '>= 2.6' # TODO - NEEDS ES8      # HTTP client library, with support for multiple backends
gem 'async-http-faraday', '>= 0.11'  
gem 'websocket-driver', '>= 0.7'

## Concurrency
gem 'concurrent-ruby', '>= 1.1.9' # Concurrency tools with futures/promises/thread pools and more
gem 'async-http', '>= 0.59' # Concurrency tools with futures/promises/thread pools and more

## Structs
gem 'sorted_set', '>= 1.0'  # Implements a variant of Set whose elements are sorted in ascending order
gem 'activesupport'  

## Parsers & validators
gem 'oj', '>= 3.13'               # Optimized JSON Parser
gem 'fast_jsonparser', '>= 0.6'   # Optimized JSON Parser
gem 'json_schemer', '>= 0.2.23'   # JSON Schema validator. Supports drafts 4, 6, and 7

## Templates
gem 'tilt', '>= 2.0'        # Templating engine
gem 'erubi', '>= 1.11'      # Template syntax

## Events generator
#gem 'croupier'

## DDD
#gem 'gauge-ruby', '>= 0.5'    # BUG: runner for Gauge BDD
gem 'test-unit',  '>= 3.5'
############ \\ MANDATORY // ############

#########################################

############ // T5 ENGINE \\ ############

## Workflow
group :engine_iodine, optional: false do
  gem 'iodine', '>= 0.7'
end

group :engine_puma, optional: false do
  gem 'puma', '>= 6.0'
end

group :engine_agoo, optional: false do
  gem 'agoo', '>= 2.15'
end

############ \\ T5 ENGINE // ############


############ // T5 CORE \\ ############

## Workflow
group :core_workflow, optional: true do
  #gem 'zeebe-client', '>= 0.16'
  gem 'zeebe-client', git: 'https://github.com/zeebe-io/zeebe-client-ruby.git', branch: 'master'
end

## In-memory/session DB
group :core_db, optional: true do
  #gem 'mdbx', '>= 0.3'
  #gem 'mdbx', git: 'https://code.martini.nu/fossil/ruby-mdbx.git', branch: 'master'  
  gem 'redis', '>= 5.0'
  gem 'async-redis',  git: 'https://github.com/EugeneIstomin/async-redis.git', branch: 'main'
  gem "hiredis", ">= 0.6.3"  
end
  
## HTTP Application server
group :core_rest, optional: true do
  gem 'grape', '>= 1.6'        # An opinionated framework for creating REST-like APIs in Ruby
  gem 'grape-path-helpers', '>= 1.7'
  gem 'grape-entity', '>= 0.10'
  gem 'grape_logging', '>= 1.8'
  gem 'grape-swagger', '>= 1.5'
  gem 'grape-swagger-entity', '>= 0.5'
  gem 'rack-cors', '>= 1.1', require: 'rack/cors'
end

############ \\ T5 CORE // ############

############ // MODULES \\ ############


##### DATABASE #####
group :db_neo4j, optional: true do
#  gem 'activegraph', git: 'https://github.com/neo4jrb/activegraph.git', branch: '10'
  gem 'neo4j-ruby-driver', '>= 1.7'
  gem 'neo4j-rake_tasks'
  gem 'neo4j'
end

group :db_elastic, optional: true do
  gem 'elasticsearch', '= 7.17.7'     # BUG: integrations for Elasticsearch 
  gem 'elasticsearch-dsl'           # BUG: API for the Elasticsearch Query DSL
end

group :db_rdbms, optional: true do
  gem 'sequel', '>= 5.62'  
  gem 'pg', '>= 1.4'
end


##### SERVICES API #####
group :api_telegram, optional: true do
  #gem 'tdlib-schema', git: 'https://github.com/seorgiy/tdlib-schema.git', branch: 'master'
  #gem 'tdlib-schema', git: 'https://github.com/Memex-Team/tdlib-schema.git', branch: 'master'
  #gem 'tdlib-schema', git: 'https://github.com/krelly/tdlib-schema.git', branch: 'master'
  gem 'tdlib-schema', git: 'https://github.com/EugeneIstomin/tdlib-schema.git', branch: 'master'
  gem 'tdlib-ruby',   git: 'https://github.com/EugeneIstomin/tdlib-ruby.git', branch: 'master'
  #gem 'tdlib-ruby', git: 'https://github.com/krelly/tdlib-ruby.git', branch: 'master'
  #gem 'telegram-bot-ruby', '>= 0.18' # BUG: wrapper for Telegram's Bot API
end

group :api_corporateDoc, optional: true do
  gem 'jira-ruby', '>= 2.2'       # API for JIRA 
  gem 'gitlab', '>= 4.19'         # client and CLI for GitLab API
  gem 'confluence-rest-api', '>= 1.1' #Confluence RESt API
end

group :api_smbDoc, optional: true do
  # gem 'notion-ruby-client', git: 'https://github.com/orbit-love/notion-ruby-client.git', branch: 'main'
end


##### BUS #####
group :bus_kafka, optional: true do
  gem 'rdkafka', '>= 0.11'
end


##### MATH #####
group :math_probability, optional: true do
  gem 'descriptive_statistics'
  #gem 'hornetseye-fftw3'
  #gem 'multiarray'
  #gem 'distribution'
end


##### PARSER #####
group :parsing_general, optional: true do
  gem 'openapi3_parser', '>= 0.9' # Open API 3 Parser/Validator 
  gem 'ox', '>= 2.14'               # Optimized XML Parser
  gem 'saxerator',    '>= 0.9'
  #gem 'smarter_csv'
end

group :parser_webscraper, optional: true do
  #gem 'watir', '>= 7.1' 
  #gem 'webdrivers', '>= 5.0' 
  #gem 'useragents'
  #gem 'webdriver-user-agent', '>= 7.8' 
  gem 'jsoner', '>= 0.0.4'          # Serialize HTML tables into JSON 
  gem 'nokogiri', '>= 1.13'         # HTML, XML, SAX, and Reader parser.
end

group :parser_ruby, optional: true do
  gem 'parser',   '>= 3.1'          # Ruby parser
  #gem 'unparser', '>= 0.6'          # Turn Ruby AST into semantically equivalent Ruby source
  gem 'sxp',      '>= 1.2'          # S-Expressions for Ruby
  #gem 'ruby-graphviz', '>= 1.2'
end


##### HTTP #####
group :http_alternatives, optional: true do
  gem 'httparty', '>= 0.20'
end


##### OPS #####
group :ops_tracing, optional: true do
  gem 'elastic-apm', '>= 4.5.0'
end

## Email
group :ops_email do
  gem 'pony', '>= 1.13'
  gem 'net-smtp', '>= 0.3'
end

group :ops_k8s, optional: true do
  gem 'kubeclient', '>= 4.9'
end

group :ops_ssh, optional: true do
  gem 'net-ssh', '>= 6.1'
  gem 'sshkey', '>= 2.0'
  gem 'ed25519', '>= 1.3'
  gem 'bcrypt_pbkdf', '>= 1.1'
end


##### SYS #####
group :sys_sidekiq, optional: true do
  gem 'sidekiq', '>= 6.4'         # Background processing 
  # gem 'sidekiq-batch', '>= 0.1'
  # gem 'sidekiq-status', '>= 1.1'
  # gem 'sidekiq-cron','>= 1.0'
  # gem 'sidekiq-rate-limiter', '>= 0.1' 
end

group :sys_grpc, optional: true do
  gem 'grpc-tools', '>= 1.50'
  gem 'grpc', '>= 1.50'
  gem 'google-protobuf', '>= 3.21'
end

##### OTHERS #####
group :misc_books, optional: true do
  gem 'googlebooks'
end

group :misc_browser, optional: true do
  gem 'browser', '>= 5.3'
end

#group :misc_speech, optional: true do
  #gem "google-cloud-speech", ">= 1.2"
#end

group :misc_multilang, optional: true do
  gem 'i18n', '>= 1.12' 
end

#group :misc_zoom, optional: true do
  #gem 'zoom_rb', '>= 0.10', git: 'https://gitlab.com/memex.team/../zoom_rb.git', branch: 'archestry'
#end
#

############ \\ MODULES // ############

########################################

############### // DEV  MODULES \\ ##############

group :dev_main, optional: true do
    #gem 'rubocop',   '>= 1.26'      # linter
    gem 'heap-profiler', git: 'https://github.com/Shopify/heap-profiler.git', branch: 'master'
end

group :dev_misc, optional: true do
  gem 'rack-unreloader', '>= 1.8'   # Allows code reloading
  gem 'awesome_print', '>= 1.9'   # Pretty print objects with style in full color with proper indentation
  # gem 'lograge', '>= 0.11'

#   gem 'rubocop-performance', '>= 1.4.1'
#   gem 'rubocop-rspec', '>= 1.37.0'
  
  gem 'yard', '> 0.9'               # Documentation tool
    
  #Testing
  gem 'simplecov', '>= 0.21'      # powerful configuration library
  gem 'derailed_benchmarks'
  
  gem 'sorbet'
  gem 'sorbet-runtime'  
  
  gem 'skunk'
  gem 'benchmark', '>= 0.2'
  gem 'ruby-prof', '>= 1.0'
  gem 'rbtrace', '>= 0.4'
  gem 'memory_profiler', '>= 1.0'
  gem 'benchmark-memory', '>= 0.2'
  gem 'activerecord-explain-analyze', '>= 0.1'
  #gem 'puma-plugin-systemd'  
end

############### \\ DEV MODULES // ##############

#### ??
#https://github.com/metainspector/metainspector
#https://github.com/intridea/multi_json
#https://github.com/rails/jbuilder
#https://github.com/grosser/fast_gettext
#https://github.com/deivid-rodriguez/pry-byebug

#gemspec
 
