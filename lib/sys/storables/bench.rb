#class HMash < ::Hashie::Mash
  #include Hashie::Extensions::Mash::DefineAccessors
  #include Hashie::Extensions::Mash::SymbolizeKeys
  ##include Hashie::Extensions::Mash::SafeAssignment
#end

#class HHashie < Hash
  ##include Hashie::Extensions::MergeInitializer
  ##include Hashie::Extensions::DeepMerge  
  ##include Hashie::Extensions::IndifferentAccess
  #include Hashie::Extensions::MethodAccess
#end
##

module RunOkBench
  
  def init 
    require 'oj'
    require 'hashie'
    require 'hashugar'
    require 'recursive-open-struct' 
    #require 'deep_open_struct'
    require 'hashr'
    require 'benchmark/ips'
    require 'benchmark/memory'
    require '../canvasFast'
    require '../canvasSimple'
    require '../discovery3'
    
    source = File.read('tg_big.json')
    hashDataset =  Oj.load(source)

  end
  benchMode = ARGV[1]
  benchRun = ARGV[2]

  #source = File.read('tg.json')
  #

  
  class Discovery ; end

  def init
    canvasFast = CanvasFast.new hashDataset
    canvasSimple = CanvasSimple.new hashDataset
    discovery3 = hashDataset.to_discovery3

    if benchMode == 'all' 
      hMash = HMash.new hashDataset
      recOStruct = RecursiveOpenStruct.new hashDataset
      hSugar = hashDataset.to_hashugar
      hHashr = Hashr.new hashDataset
    end


    canvasFast_dataset = CanvasFast.new hashDataset
    canvasSimple_dataset = CanvasSimple.new hashDataset
    discovery3_dataset = hashDataset.to_discovery3

    if benchMode == 'all' 
      recOStruct_dataset = RecursiveOpenStruct.new hashDataset
      #hMash_dataset = HMash.new hashDataset
      #hSugar_dataset = hashDataset.to_hashDatasetugar
      hHashr_dataset = Hashr.new hashDataset
    end


    if ARGV[0] == 'bench'
      
      if benchRun == 'all' or benchRun == 'init'
        config = {ips:    {:time => 1, :warmup => 1},
                  memory: {:time => 1}
                }
        benchV = 'ыв90фыв7а8фыва78ыва76т*%;У:Г*(ЛОр'

        puts "\n########################### BENCHMARKING ###########################"

        puts "\n\n###### INIT #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            x.config(:time => 1, :warmup => 1) if op == :ips
            
            x.report("hash")          { a = Oj.load(source) }    
            x.report("canvasFast")    { a = CanvasFast.new(Oj.load(source))}          # 3 (13.29x)   
            x.report("canvasSimple")  { a = CanvasSimple.new(Oj.load(source))}        # 2 (5.66x)
            x.report("discovery3")    { a = Discovery3.new(Oj.load(source))}      # 1 (1.66x)                                                                          
                                              
            if benchMode == 'all'
              x.report("hSugar")     { a = (Oj.load(source)).to_hashugar }
              x.report("recOStruct") { a = RecursiveOpenStruct.new(Oj.load(source)) }
              x.report("hHashr")     { a = Hashr.new(Oj.load(source)) }
              #x.report("hMash")     { HMash.new hashDataset }  
            end
                                              
            x.compare!
          end
        end                                             
      end
                                            

      if benchRun == 'all' or benchRun == 'simpleRead'

        puts "\n\n###### SIMPLE READ #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            x.config(:time => 1, :warmup => 1) if op == :ips
            
            x.report("hash")            { a = hashDataset['update_id']}    
            x.report("canvasFast")      { a = canvasFast._update_id }           # 1 (2.48x)
            x.report("canvasSimple")    { a = canvasSimple._update_id }                 # 3 (13.57x)
            x.report("discovery3")      { a = discovery3._update_id}              # 2 (12.75x)

            if benchMode == 'all'
              x.report("hSugar")     { a = hSugar.update_id }
              x.report("recOStruct") { a = recOStruct.update_id }
              x.report("hHashr")     { a = hHashr.update_id }
            end
                                              
            x.compare!
          end
        end  
      end
          
                                              
      if benchRun == 'all' or benchRun == 'nestedRead'
                                              
        puts "\n\n###### NESTED READ #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            x.config(:time => 1, :warmup => 1) if op == :ips
            
            x.report("hash")            { a = hashDataset['message']['chat']['text']}    
            x.report("canvasFast")      { a = canvasFast._message._chat._text }     # 1 (2.95x)
            x.report("canvasSimple")    { a = canvasSimple._message._chat._text }           # 3 (19.26x)
            x.report("discovery3")      { a = discovery3._message._chat._text }       # 2  (18.15x)                                
                                              
            if benchMode == 'all'                                         
              x.report("hSugar")     { a = hSugar.message.chat.text }                                   
              x.report("recOStruct") { a = recOStruct.message.chat.text }
              x.report("hHashr")     { a = hHashr.message.chat.text }
            end
                                              
            x.compare!
          end
        end   
      end
                                              
      if benchRun == 'all' or benchRun == 'nestedReadReflection'
                                              
        puts "\n\n###### NESTED READ USING REFLECTION #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            x.config(:time => 1, :warmup => 1) if op == :ips
            
            x.report("hash")            { hashDataset['message']['chat']['aa'] = {}; hashDataset['message']['chat']['aa']['bb'] = {}; a = hashDataset['message']['chat']['aa']['bb']['cc'] = {}}    
            x.report("canvasFast")      { a = canvasFast._message._chat._aa._bb._cc }     # 1 (better than hash)
            x.report("canvasSimple")    { a = canvasSimple._message._chat._aa._bb._cc }       # 2 (6.73x)
            #x.report("discovery3")      { a = discovery3._message._chat._aa._bb._cc }                                           
                                              
            if benchMode == 'all'                                         
              x.report("hSugar")    { hSugar.message.chat.aa = Hashugar.new({}); hSugar.message.chat.aa.bb = Hashugar.new({}); a = hSugar.message.chat.aa.bb.cc = Hashugar.new({}) }                                   
              x.report("oStruct")   { recOStruct.message.chat.aa = RecursiveOpenStruct.new; hSugar.message.chat.aa.bb = RecursiveOpenStruct.new; a = hSugar.message.chat.aa.bb.cc = RecursiveOpenStruct.new({})}
              x.report("hHashr")    { hHashr.message.chat.aa = Hashr.new({}); hSugar.message.chat.aa.bb = Hashr.new({}); a = hSugar.message.chat.aa.bb.cc = Hashr.new({}) }
            end
                                              
            x.compare!
          end
        end   
      end                                           
                                              
                                              
      if benchRun == 'all' or benchRun == 'nestedWrite'
      
        puts "\n\n###### NESTED WRITE #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            x.config(:time => 1, :warmup => 1) if op == :ips
            
            x.report("hash")            { hashDataset['message']['chat']['aa'] = {}; hashDataset['message']['chat']['aa']['bb'] = {}; hashDataset['message']['chat']['aa']['bb']['cc'] = hashDataset['message']['chat']['text'] }    
            x.report("canvasFast")      { canvasFast._message._chat._aa._bb._cc = canvasFast._message._chat._text }     # 1 (1.37x)
            x.report("canvasSimple")    { canvasSimple._message._chat._aa._bb._cc = canvasSimple._message._chat._text }       # 2 (10.41x)
            #x.report("discovery3")      { discovery3._message._chat._aa._bb._cc = discovery3._message._chat._text }                                           
                                              
            if benchMode == 'all'                                         
              x.report("hSugar")    { hSugar.message.chat.aa = Hashugar.new({}); hSugar.message.chat.aa.bb = Hashugar.new({}); hSugar.message.chat.aa.bb.cc = hSugar.message.chat.text }                                   
              x.report("oStruct")   { recOStruct.message.chat.aa = RecursiveOpenStruct.new; hSugar.message.chat.aa.bb = RecursiveOpenStruct.new; hSugar.message.chat.aa.bb.cc = recOStruct.message.chat.text }
              x.report("hHashr")    { hHashr.message.chat.aa = Hashr.new({}); hSugar.message.chat.aa.bb = Hashr.new({}); hSugar.message.chat.aa.bb.cc = hHashr.message.chat.text }
            end

            x.compare!
          end
        end
      end
                                                              
            
      if benchRun == 'all' or benchRun == 'eachWrite'
                                              
        puts "\n\n###### .EACH WRITE #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            #x.config(config[op])
            x.config(:time => 1, :warmup => 1) if op == :ips
                                              
                                              
            x.report("hash")            { a = hashDataset['message'].each { |k, v| k = v } }    
            x.report("canvasFast")      { a = canvasFast._message.each { |k, v| k = v } }           # 3 (11.11x)
            x.report("canvasSimple")    { a = canvasSimple._message.each { |k, v| k = v } }     # 1 (2.28x)
            x.report("discovery3")      { a = discovery3._message.each { |k, v| k = v } }         # 2 (4.47x)                                  
                                              
            if benchMode == 'all'                                         
              x.report("hSugar")     { a = hSugar.message.each { |k, v| k = v } }                                   
              x.report("recOStruct") { a = recOStruct.message.each { |k, v| k = v } }
              x.report("hHashr")     { a = hHashr.message.each { |k, v| k = v } }
            end
                                                                                      
            x.compare!
          end
        end
      end

                                              
      if benchRun == 'all' or benchRun == 'key'
                                              
        puts "\n\n###### KEY FIND #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            x.config(:time => 1, :warmup => 1) if op == :ips
            
            x.report("hash")            { a = hashDataset['message']['chat'].key?('text')}    
            x.report("canvasFast")      { a = canvasFast._message._chat.key?(:text) }     # 1 (7.35x)
            x.report("canvasSimple")    { a = canvasSimple._message._chat.key?(:text) }           # 3 (8.95x)
            x.report("discovery3")      { a = discovery3._message._chat.key?('text') }       # 2  (8.39x)                                
                                              
            if benchMode == 'all'                                         
              x.report("hSugar")     { a = hSugar.message.chat.key?('text') }                                   
              #x.report("recOStruct") { a = recOStruct.message.chat.key?('text') }
              x.report("hHashr")     { a = hHashr.message.chat.key?('text') }
            end
                                              
            x.compare!
          end
        end   
      end
                                              

      if benchRun == 'all' or benchRun == 'toHash'

        puts "\n\n###### .TO_H #############"
        [:ips, :memory].each do |op|
        puts "\n\## STEP: #{op} =========="                                       
          Benchmark.send(op)  do |x| 
            #x.config(config[op])
            x.config(:time => 1, :warmup => 1) if op == :ips
                                            
            x.report("hash")            { a = hashDataset.to_h }    
            x.report("canvasFast")      { a = canvasFast.to_h; }         # 3 (223.19x)
            x.report("canvasSimple")    { a = canvasSimple.to_h }       # 2 (99.19x)
            x.report("discovery3")      { a = discovery3.to_h }     # 1 (10.83x)                                  
                                              
            if benchMode == 'all'                                         
              x.report("hSugar")     { a = hSugar.to_h }                                   
              x.report("recOStruct") { a = recOStruct.to_h }
              x.report("hHashr")     { a = hHashr.to_h }
            end
                                              
            x.compare!
          end
        end      
      end
    end
                                      

  end
end
