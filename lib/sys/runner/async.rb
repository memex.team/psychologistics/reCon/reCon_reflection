# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Runner
    module Async
      class << self
        
        def runJob (jobs, hIn)
        Async do |task|       
          # p "## Start jobId #{hIn._jobId} (#{hIn._i}i on #{hIn._iSec}s) with timer #{jobs[hIn._jobId]._timer} (#{DateTime.now})"
            task.async do |c|
              res = {}
              begin
                p jobs[hIn._jobId]._runner._class.constantize.send(jobs[hIn._jobId]._runner._method, jobs[hIn._jobId]._args)
              rescue => e
                p e
              end
            end
          end
          #
          jobs[hIn._jobId]._lastRun = DateTime.now.strftime('%s').to_i
        end        
        
        def backgroundJobs(jobs)
          i = 0
          iteration = 0.2
          res = O.k
          @thr = Thread.new do
            ::Async::Reactor.run do
              sleep 1
              loop do
                sleep iteration
                iSec = iteration * i
                jobs.each do |jobId, _jobProps|
                  runJob(jobs, O.k(jobId: jobId, i: i, iSec: iSec)) unless jobs[jobId].key?(:lastRun)
                  runJob(jobs, O.k(jobId: jobId, i: i, iSec: iSec)) if ( (jobs[jobId].key?(:lastRun)) && (DateTime.now.strftime('%s').to_i > jobs[jobId]._lastRun + jobs[jobId]._timer) )
                end                  
                # p "## End (#{i}i on #{iSec}s)"
                #::Env::Base::Runner.async._cycle
                i += 1
              end
            end
          end
        end
        
        
        def pubSubRedisAsync(r) 
          i = 0
          res = O.k
          p 111
          @thr = Thread.new do
            ::Async::Reactor.run do
                sleep 1
                Async do |task|   
                  condition = ::Async::Condition.new
                  subscriber = task.async do
                    res = {}
                      loop do                    
                      r.subscribe 'bus' do |c|
                        condition.signal
                        type, name, message = c.listen
                        msg = O.k(Oj.load(message))
                        case msg._type.to_sym
                        when :call
                          msg._call._c.constantize.send(msg._call._method.to_sym, msg._args)
                        when :event
                          pp msg.to_h
                        end
                      end
                      i += 1
                      p "msg ##{i}"
                  rescue => e
                    p e
                  end
                end
              end
            end
          ensure
            r.close            
          end
          
          p 222
        end
        
        def tdlib(t) 
          i = 0
          res = O.k
 
          @thr = Thread.new do
         tgSess = ::TD::ClientV2.new(**t)
          tgSess.connect          
          p tgSess.get_me
            # ::Async::Reactor.run do
            #     sleep 1
            #     loop do 
            #     Async do |task|   
                        begin  
                          state = nil                          
                          tgSess.on(TD::Types::Update::NewMessage)  do |update|
                            state = update
                          end
                          loop do
                            sleep 0.5
                            p state 
                          end
                        end
            #           i += 1
            #           p "msg ##{i}"
            #       rescue => e
            #         p e
              #     end
              #   end
              # end
            end
          # ensure
          #   tgSess.close            
          # end
        end        
     
        
        def ping
          p "ping"
        end
        
      
      end
    end
  end
end
