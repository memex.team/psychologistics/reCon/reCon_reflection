# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
require 'protocol/http/header/authorization'
require 'async/http/internet'
require 'async/http/client'     
          
module Memex::Reflection
  module Http
    module Post
      class << self
        
        def asyncBusCall hIn
          res = O.k
          res._result = false
          #
          Async do |task|
            #res._sys._timer = hIn._timer if hIn.key?(:timer)
            #res._sys._urlHash = Digest::SHA1.hexdigest(hIn._url)
            #
            c = ::Async::HTTP::Internet.new   
            hIn._url = hIn._url + "&msgFrom=#{hIn._msg._from}&msgTo=#{hIn._msg._to}&msgId=#{hIn._msg._id}"
            #sleep 100
            begin
              task.with_timeout(0.3) do            
                hIn._url = "http://#{ENV['reConHost']}:#{ENV['reConPort']}#{hIn._url}" if hIn._url[0] == '/'
                headers = [['accept', 'application/json'], ['Content-Type', 'application/json'], ['User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36']]          
                #
                response = c.post(hIn._url, headers, [Oj.dump(hIn._msg._msg._post.to_h)])
                p response.read
                res._result = O.k(Oj.load(response.read, bigdecimal_load: :float))
              end
            rescue Async::TimeoutError
              true
            ensure
              c.close
            end
            #
          end
          #
          res          
        end   

        def sync hIn
          res = O.k       
          hIn._url = "http://#{ENV['reConHost']}:#{ENV['reConPort']}#{hIn._url}" if hIn._url[0] == '/'
          #
          uri = URI(hIn._url)
          http = Net::HTTP.new(uri.host, uri.port)
          http.read_timeout = 6000
          http.use_ssl = true if uri.scheme == 'https'
          #
          begin
            req = Net::HTTP::Get.new(uri.request_uri, {'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36'})            
            rsp = http.request(req)
            begin
              res._result = O.k(Oj.load(rsp.body))
            rescue
              res._result = rsp.body              
            end
          rescue => e
            p e
            p rsp.body
          end
          res
          #
        end
      
      end
    end
  end
end


