# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Events
    module Flow
      class << self
        def stdoutJson(hIn)
          ios = IO.new(STDOUT.fileno)
          ios.write("#{hIn[:json]}\n")
          ios.close
        end

        def stdoutArray(hIn)
          ios = IO.new(STDOUT.fileno)
          hIn.each do |k, a|
            ios.write("### #{k}\n")
            if a.is_a?(Array)
              a.each { |line| ios.write("#{line}\n") }
            else
              ios.write("#{a}\n")
            end
          end
          ios.close
        end
        

        def verify(hIn)
          md = {}

          case hIn[:opt]
          when :console
            stdoutArray(hIn[:message])
          else
            if hIn[:message].is_a?(Hash)
              md['logtoken'] = hIn[:logtoken].to_s
              md.merge!(hIn[:message])
              hIn[:message].to_s
            else
              md['logtoken'] = hIn[:logtoken].to_s
              md['msg'] = hIn[:message].to_s
            end

            stdoutJson(json: JSON.pretty_generate(md))
          end
        end

        def error(hIn)
          verify(level: :error, domain: hIn[:domain], message: hIn[:message], opt: hIn[:opt], logtoken: hIn[:logtoken])
        end

        def warn(hIn)
          verify(level: :info, warn: hIn[:domain], message: hIn[:message], opt: hIn[:opt], logtoken: hIn[:logtoken])
        end

        def info(hIn)
          verify(level: :info, domain: hIn[:domain], message: hIn[:message], opt: hIn[:opt], logtoken: hIn[:logtoken])
        end

        def debug(hIn)
          verify(level: :debug, domain: hIn[:domain], message: hIn[:message], opt: hIn[:opt], logtoken: hIn[:logtoken])
        end
      end
    end

    module Biz
      class << self
        def error(message, opt = false) = Memex::Reflection::Events::Flow.error(domain: :biz, message:, opt:)
        def warn(message, opt = false) = Memex::Reflection::Events::Flow.warn(domain:  :biz, message:, opt:)
        def info(message, opt = false) = Memex::Reflection::Events::Flow.info(domain:  :biz, message:, opt:)
        def debug(message, opt = false) = Memex::Reflection::Events::Flow.debug(domain: :biz, message:, opt:)
      end
    end

    module App
      class << self
        def error(logtoken, message, opt = false)
          Memex::Reflection::Events::Flow.error(domain: :app, message:, opt:, logtoken:)
        end

        def warn(logtoken, message, opt = false)
          Memex::Reflection::Events::Flow.warn(domain:  :app, message:, opt:, logtoken:)
        end

        def info(logtoken, message, opt = false)
          Memex::Reflection::Events::Flow.info(domain:  :app, message:, opt:, logtoken:)
        end

        def debug(logtoken, message, opt = false)
          Memex::Reflection::Events::Flow.debug(domain: :app, message:, opt:, logtoken:)
        end
      end
    end

    module Ops
      class << self
        def error(message, opt = false) = Memex::Reflection::Events::Flow.error(domain: :ops, message:, opt:)
        def warn(message, opt = false) = Memex::Reflection::Events::Flow.warn(domain:  :ops, message:, opt:)
        def info(message, opt = false) = Memex::Reflection::Events::Flow.info(domain:  :ops, message:, opt:)
        def debug(message, opt = false) = Memex::Reflection::Events::Flow.debug(domain: :ops, message:, opt:)
      end
    end
  end
end

module Log
  include Memex::Reflection::Events
end

module De
  class << self
    def bug(logtoken, message)
      Memex::Reflection::Events::App.debug(logtoken, message, opt = false)
    end
  end
end
