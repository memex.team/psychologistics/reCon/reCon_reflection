# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######\

module Pubsub
  module GenProto
    class << self
      
      def event msg, type
        res = O.k({
          timestamp: DateTime.now.strftime('%Q'),
          token:    ENV['busToken'],
          type:     type, 
          handler:  msg[:handler],
          state:    msg[:state],
          domain:   ENV['memexDomain'], 
          from:     ENV['memexContainer'], 
          msg:      msg[:msg]
        })
        msg.key?(:to)? res._to = msg[:to] : res._to = ENV['memexContainer']
        res._parent = msg[:parent] if msg.key?(:parent)      
        res._pid = Digest::SHA1.hexdigest("#{res._domain}:#{res._from}:#{res._to}:#{res._type}:#{res._handler}")
        res._id = Digest::SHA1.hexdigest("#{res._timestamp}::#{res._type}::#{res._domain}::#{res._from}::#{res._to}")
        res
      end
      
      def call msg, type
        res = event(msg, type)
        res._call = msg[:call] 
        res._args = msg[:args]
        res._pid = Digest::SHA1.hexdigest("#{res._pid}:#{res._callURL}:#{res._args}")
        res
      end
      
    end
  end
end

