# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
if T5_ENGINE == 'iodine'

  module Pubsub
    class RedisIodine
      class << self
        
        def subscriptionCaller
          return unless @performed_once_and_only_once.try_lock
          # Iodine.subscribe("#{@memexDomain}::workflow", match: :redis, &Messagebus.method(:runAsyncTask))
          # Iodine.subscribe("#{@memexDomain}::calls",    match: :redis, &Messagebus.method(:runAsyncTask))
          # Iodine.subscribe("#{@memexDomain}::events",   match: :redis, &Messagebus.method(:runAsyncTask))

          @thread = Thread.new do
          while(@flag) do
              (@queue.shift).call
            end
          end
        end
        
        def runAsyncTaskLoop(ch, msg)
          #i = 0
          #iteration = 0.5
          #res = O.k
          
          #@thr = Thread.new do               
            #::Async::Reactor.run do
              #sleep 1
              #loop do
                #sleep iteration
                #iSec = iteration * i              
                ##
                #Async do |task|       
                  #task.async do |c| 
  Iodine.run do
    runAsyncTask(ch, msg) unless Iodine.master?                  
  end        
                    
                    #begin
                      ##p 1
                    #rescue => e
                      #p e
                    #end
                  #end
                #end
              #end
            #end
          #end
        end
        
        def runAsyncTask(ch, msg)
          return if Iodine.master? 
          msg = Oj.load(msg, symbol_keys: true)
            @queue << Proc.new do
              if msg[:to].to_s == ENV['memexContainer'].to_s
                case ch
                when "#{@memexDomain}::workflow"
                Iodine.run {  Memex::Reflection::Workflow::Zeebe.sendMessage(O.k(z: @zeebeClient, name: msg[:msg][:name], cKey: msg[:msg][:cKey], vars: msg[:msg][:vars])) }
                #
                when "#{@memexDomain}::events"

                          #Iodine.run { p "EVENT :: #{msg}" }
                          a = O.k
                          a._qLength  = @queue.length
                          a._qWaiting = @queue.num_waiting
                          #p a
                          #sleep msg[:state]
                          p "Sleeped #{msg[:state]} secs"
                          if  ENV['workflowMode'] == 'true'
                            Iodine.run {  Memex::Reflection::Workflow::Zeebe.sendMessage(O.k(z: @zeebeClient, name: msg[:msg][:name], cKey: msg[:msg][:cKey], vars: msg[:msg][:vars])) }                              
            
                  end
                #
                when "#{@memexDomain}::calls"
                  msg[:channel]  = ch
                  msg[:qLength]  = @queue.length
                  msg[:qWaiting] = @queue.num_waiting
                  #
                  res = false
                  url = "#{msg[:callURL]}?"
                  msg[:args].each do |k, v|
                    url = url + "#{k}=#{v}&"
                  end
                  url = url + "parentId=#{msg[:parent][:id]}&parentPid=#{msg[:parent][:pid]}" if msg.key?(:parent)
                  url = url + "parentId=#{msg[:parent][:id]}&parentPid=#{msg[:parent][:pid]}" if msg.key?(:parent)
                  #                  
                  #
                  Iodine.run do
                    callChannel = "#{@memexDomain}::calls::#{msg[:id]}"
                    Iodine.publish(callChannel, 'true')                   
                  end
                  #Memex::Reflection::Http::Post.asyncBusCall(O.k(url: url, msg: msg))              
                end
                #
              end
          end
        end    

        
        def sendWorkflow msg
          resMsg = makeMsg(msg, :call)
          res = Iodine.publish("#{@memexDomain}::workflow", Oj.dump(resMsg.to_h))
          res
        end
        
        def sendEvent msg
          e = Pubsub::GenProto.event(msg, :event)
          Async do |task|   
            condition = ::Async::Condition.new
              subscriber = task.async do
                ::Re::Db::Redis.rAsyncConnect(:PubSub).publish('bus',  e.to_h.to_json)
              end
          end
          e
        end
        
        def sendCall msg
          e = Pubsub::GenProto.call(msg, :call)
          Async do |task|   
            condition = ::Async::Condition.new
              subscriber = task.async do
                ::Re::Db::Redis.rAsyncConnect(:PubSub).publish('bus', e.to_h.to_json)
              end
          end
          e
        end      
            
        
        def cleanup
          return unless @thread
          @flag = false
          @queue << Proc.new { puts "Async worker done." }
          @thread.join
          puts "Async worker finished." 
        end    
        
      end
    end
  end

end
