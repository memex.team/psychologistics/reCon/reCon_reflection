# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######\
if T5_ENGINE == 'iodine'

    class Messagebus < Pubsub::RedisIodine
        @thread = nil
        @queue = Queue.new
        @performed_once_and_only_once = Mutex.new
        @flag = true
        @memexDomain = ENV['memexDomain']
        # @zeebeClient = ::Zeebe::Client::GatewayProtocol::Gateway::Stub.new("#{ENV['zeebeHost']}:#{ENV['zeebePort'].to_i}", :this_channel_is_insecure)
        #
        # Iodine.on_state(:enter_master, &Messagebus.method(:subscriptionCaller))
        Iodine.on_state(:on_finish,    &Messagebus.method(:cleanup))
    end

end
