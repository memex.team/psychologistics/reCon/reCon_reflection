# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
if T5_CORE.include?('rest')
  module Grape
    module Extensions
      module Ok
        module ParamBuilder
          extend ::ActiveSupport::Concern
          included do
            namespace_inheritable(:build_params_with, Grape::Extensions::Ok::ParamBuilder)
          end

          def params_builder
            Grape::Extensions::Ok::ParamBuilder
          end

          def build_params
            params = ::Ok.new
            params._apiArgs = ::Ok.new(rack_params)
            params._apiArgs._json = O.k(Oj.load(params._apiArgs._json)) if params._apiArgs.key?(:json)
            params._apiPath = ::Ok.new(grape_routing_args) if env[Grape::Env::GRAPE_ROUTING_ARGS]
            params
          end
        end
      end
    end
  end
end
