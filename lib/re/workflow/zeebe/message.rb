# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Workflow   
    module Zeebe
      #class Localstore
        class << self
          
         def sendMessage hIn
          res = hIn._z.publish_message(::Zeebe::Client::GatewayProtocol::PublishMessageRequest.new(
            name: hIn._name,
            correlationKey: hIn._cKey,
            variables: Oj.dump(hIn._vars)
            ))
          res
        end

        
        end
      #end
    end
  end
end
