# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Workflow   
    module Zeebe
      #class Localstore
        class << self
          
          def deploy hIn
          res = hIn._z.deploy_process(Zeebe::Client::GatewayProtocol::DeployProcessRequest.new({processes: [{name: 'bpmnName', definition: file_content = File.open("current.bpmn", "r:UTF-8", &:read)}] }))
          end

          def start hIn
            processRunArgs = { bpmnProcessId: 'bpmnName', 
                                  version: -1, 
                                  variables: {aa: 12, bb: 23}.to_json
                                }
            run = hIn._z.create_process_instance(Zeebe::Client::GatewayProtocol::CreateProcessInstanceRequest.new(processRunArgs))
          end

        
        end
      #end
    end
  end
end
