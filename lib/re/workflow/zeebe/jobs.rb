# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Workflow   
    module Zeebe
      #class Localstore
        class << self
          
          def remapArgs(pattern, map)
            pattern.gsub(/\{{(.*?)\}}/) do |key| 
              map[key[/\{{(.*?)\}}/, 1] ]
            end
          end
          
          
          def activateJobs hIn
            hIn.key?(:maxJobs) ? maxJobsToActivate = hIn._maxJobs : maxJobsToActivate = 1
            hIn.key?(:requestTimeout) ? requestTimeout = hIn._requestTimeout : requestTimeout = 5
            #            
            hIn._z.activate_jobs(::Zeebe::Client::GatewayProtocol::ActivateJobsRequest.new(
              type:   "T5::#{ENV['memexContainer']}",
              worker: 't5_worker',
              timeout: 43200000,
              maxJobsToActivate: maxJobsToActivate,
              requestTimeout: requestTimeout
              )) do |a|
                a['jobs'].each do |job|
                  begin
                    #De.bug(false, job.to_h)
                    callAndCompleteJob(O.k(z: hIn._z, job: job))
                  #rescue
                    #p 222222
                    ##failJob(hIn, job)
                    #throwError(O.k(z: hIn._z, jobKey: job['key']))
                  rescue => e
                    p e
                  end
                end
            end
          end          

                              
          def callAndCompleteJob hIn
            hIn.key?(:vars) ? message = hIn._vars : message = "OK"
            #  
            args = O.k(Oj.load(hIn._job['customHeaders']))
            variables = Oj.load(hIn._job['variables'])           
            #
            args.key?(:sync) ? mode = :sync : mode = :asyncBusCall
            #
            call = args._call.split('?')
            callSender = call[0].split('.')
            callArg = call[1].split('=')
            begin
              jobRes = "Memex::Containers::#{ENV['memexContainer'].capitalizeFirst}::#{callSender[0]}".constantize.send(callSender[1].to_sym, O.k({callArg[0] => callArg[1]}))
            rescue => e
              p e
            end
            #jobRes = Memex::Reflection::Http::Get.send(mode, O.k(url: remapArgs(args[mode], variables) + "?json=#{Oj.dump(variables)}"))
            De.bug('jobRes', "Job result: #{jobRes}")

            #
            res = hIn._z.complete_job(::Zeebe::Client::GatewayProtocol::CompleteJobRequest.new(
            jobKey:   hIn._job['key'],
            variables: Oj.dump({'result' => jobRes._result.to_h})
            )) 
            #
            res
          end

                              
          def failJob hIn
            hIn.key?(:errorMessage) ? message = hIn._message : message = "failJob!!"
            #
            res = hIn._z.fail_job(::Zeebe::Client::GatewayProtocol::FailJobRequest.new(
            jobKey:   hIn._jobKey,
            retries: -1,
            errorMessage: message,
            retryBackOff: 3                                                                           
            )) 
            #
            res
          end
          

          def throwError hIn
            hIn.key?(:errorMessage) ? message = hIn._message : message = "throwError!!"
            #            
            res = hIn._z.throw_error(::Zeebe::Client::GatewayProtocol::ThrowErrorRequest.new(
            jobKey:   hIn._jobKey,
            errorCode: 'base',
            errorMessage: message
            )) 
            #
            res
          end          
        
        end
      #end
    end
  end
end
