# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######  
          
module Memex::Reflection
  module Math
    module Distribution
      class << self
        
        @factor = 0
        
        def genAssets hIn
        
            hIn.key?(:asset) && hIn._asset.key?(:samples)       ? samples    = hIn._asset._samples.to_i       : samples = 1000
            hIn.key?(:asset) && hIn._asset.key?(:multiplicator) ? multiplicator = hIn._asset._samples.to_i    : multiplicator = 10
            hIn.key?(:asset) && hIn._asset.key?(:distribution)  ? distribution  = hIn._asset._distribution    : distribution = 'exponential'
            hIn.key?(:asset) && hIn._asset.key?(:factor)        ? @factor = hIn._asset._factor.to_f : @factor = 0.3
            hIn.key?(:asset) && hIn._asset.key?(:model)         ? model  = hIn._asset._model  : model = hIn._assetDefaults._model
                        #
            res = O.k
            sampleQuoter1 = []
            sampleQuoter2 = []
            sampleQuoter3 = []
            sampleQuoter4 = []
            i = 0
            countPerQuoter = samples / 4
            #
            set = Croupier::Distributions.send(distribution, {lambda: @factor} )
            setSampled = set.generate_sample(samples).sort
            #
            setSampled.each do |n| 
              num = (n * multiplicator).floor(2)
              case 
              when i <= countPerQuoter
                sampleQuoter1 << num
              when i <= countPerQuoter*2
                sampleQuoter2 << num
              when i <= countPerQuoter*3
                sampleQuoter3 << num
              when i <= countPerQuoter*4
                sampleQuoter4 << num
              end             
              i += 1
            end
            #
            #res._sampleQ4 = sampleQuoter4
            #res._sampleQ3 = sampleQuoter3
            #res._sampleQ2 = sampleQuoter2
            #res._sampleQ1 = sampleQuoter1
            res._raw._a._mean = (sampleQuoter4.sum / countPerQuoter).floor(2)
            res._raw._a._max  = sampleQuoter4.max
            res._raw._a._min  = sampleQuoter4.min
            res._raw._a._median = getMedian(sampleQuoter4)
            
            res._raw._b._mean = (sampleQuoter3.sum / countPerQuoter).floor(2)
            res._raw._b._max  = sampleQuoter3.max
            res._raw._b._min  = sampleQuoter3.min
            res._raw._b._median = getMedian(sampleQuoter3)
            
            res._raw._c._mean = (sampleQuoter2.sum / countPerQuoter).floor(2)
            res._raw._c._max  = sampleQuoter2.max
            res._raw._c._min  = sampleQuoter2.min
            res._raw._c._median = getMedian(sampleQuoter2)
            
            res._raw._d._mean = (sampleQuoter1.sum / countPerQuoter).floor(2)
            res._raw._d._max  = sampleQuoter1.max
            res._raw._d._min  = sampleQuoter1.min
            res._raw._d._median = getMedian(sampleQuoter1)            
            #
            res._raw.each do |assetScore, _rawScores|              #
              rangeFullPercent = (res._raw[assetScore]._max - res._raw[assetScore]._min)                            
              meanDev = ((res._raw[assetScore]._mean - res._raw[assetScore]._min)/rangeFullPercent).round(2)
              res._raw[assetScore]._meanDev = meanDev
              #
              medianDev = ((res._raw[assetScore]._median - res._raw[assetScore]._min)/rangeFullPercent).round(2) 
              res._raw[assetScore]._medianDev = medianDev
              #                        
            end   
            #
            res._models = genModels(O.k(data: res, incoming: hIn))
            #
            res._message = true
            res
          end  
          
          
        def getMedian a
          midpoint = a.length / 2
          a.length.even? ? res = a[midpoint-1, 2].sum / 2.0 : res = a[midpoint]
          res
        end
        
        def genModels hIn
          #
          res = O.k          
          hIn._data._raw.each do |assetScore, rawScores|  
            rangeFullPercent = (rawScores._max - rawScores._min)
            ## Notsobad
            rangeMedianPercent = (rawScores._median - rawScores._min)
            rangeMeanPercent = (rawScores._mean - rawScores._min)
            rangeMMPercent = (rawScores._median - rawScores._mean)* @factor
            rangeMMPercent = 1 if rangeMMPercent == 0 
            
            res._models._notsobad[assetScore] = ((rangeFullPercent - rangeMedianPercent/rangeMMPercent) + rangeMedianPercent).round(2)
            #
          end
          res
        end
          
      end
    end
  end
end


