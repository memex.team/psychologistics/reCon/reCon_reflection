# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Db   
    module Mdbx
      #class Localstore
        class << self
    
          def push hIn
            data = false
            begin
              MDBX::Database.open(localStorage(hIn[:ns][:context])._path, localStorage._opts) do |session|
                session.collection(hIn[:ns][:db])
                session.transaction do |call|
                  data = pushDataHandler(hIn)
                  call[data[:k]] = data[:v]
                end             
              end    
              res = true
            rescue => e
              Log::App.error(false, "LocalStorage Push ERROR: #{e}, data: #{data}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end   
            #
            res
          end    
          
          def pushContext hIn
            #De.bug('hIn', hIn)
            begin
              session.collection(hIn[:ns][:db])
              hIn[:db].transaction do |call|
                data = pushDataHandler(hIn)
                call[data[:k]] = data[:v]
              end             
              res = true
            rescue => e
              Log::App.error(false, "LocalStorage Push ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end   
            #
            res
          end             
          
          
          def pushDataHandler hIn
            res = {}
            #p hIn[:value]
            case hIn[:value].class.to_s
            when 'Ok', 'O'
              res[:v] = hIn[:value].to_h
            else
              res[:v] = hIn[:value]
            end
            #if hIn[:ns][:prefix] == O.k
            res[:k] = "#{hIn[:ns][:prefix]}::#{hIn[:key].to_s}"
            #
            res
          end
          
            
               
        end
      #end
    end
  end
end
