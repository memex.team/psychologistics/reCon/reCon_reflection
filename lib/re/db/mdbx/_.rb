# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Db   
    module Mdbx
      #class Localstore
        class << self
          

          def open
            res = MDBX::Database.open(localStorage(:log)._path, localStorage._opts)
          end
          
          def context hIn
            res = O.k  
            c = myCon(hIn._context)._conf.localStore
            res._context = c._context
            #p hIn
            res._space   = hIn._space.to_s
            res._type = hIn._type.to_s
            #
            res._db = "#{res._space}::#{res._type}"
            hIn._mod.to_s == "#<Ok>" ?  res._mod = "main" : res._mod = hIn._mod.to_s
            res._prefix = res._mod
            #
            res
          end
          
                  
          def dropNs hIn
            Log::App.debug(false, hIn)
            res = false
            begin
              MDBX::Database.open(localStorage(hIn[:ns][:context])._path, localStorage._opts) do |session|                                       
                session.collection(hIn[:ns][:db])
                session.transaction do |call|
                  del = []            
                  #
                  call.each_key do |k|
                    
                    myKey = keySalt(k)
                    #del << k if myKey._prefix == O.k
                    del << k if myKey._prefix == hIn[:ns][:prefix]                 
                  end
                  del.each {|k| call.delete(k) }
                  Log::App.debug(false, "DELETED #{del.count} records")                  
                end
              end
              #
            rescue => e
              Log::App.error(false, "LocalStorage Drop ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end 
            #
            res
          end  
          
          
          def delete hIn
            #Log::App.debug('hIn', hIn)
            key = "k_#{hIn._key.to_s}"
            #
            begin
              MDBX::Database.open(::Env::Base::Store.local._pathData, ::Env::Base::Store.local._opts) do |session|
                session.collection(hIn._ns)                                       
                session.transaction do |call|
                  call.delete(key)
                end
              end    
              res = true
            rescue => e
              Log::App.error(false, "LocalStorage Delete ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end   
            #
            res
          end            
          
          
          #https://martini.nu/docs/ruby-mdbx/MDBX/Database.html#method-i-collection
          #include(key)
          #keys
          #statistics
                      
          ################
          def dumpFix  ## TO DO
            res = O.k
            
            begin
              namespaces = []
              localStorage.database.keys.each do |namespace|
                namespaces << namespace
              end
              
              namespaces.each do |namespace|
                calls = localStorage.database(namespace.to_s, create: true)
                calls.each do |key, value|
                  res[namespace][key] = value.force_encoding('UTF-8')
                end
              end
              
              #end
            rescue => e
              Log::App.error(false, "LocalStorage Dump ERROR: #{e}")
            end

            res
          end                      
              
        end
      #end
    end
  end
end

#def store
  #::Re::Db::Mdbx
#end


