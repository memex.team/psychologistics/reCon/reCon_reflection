# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Db   
    module Mdbx
      #class Localstore
        class << self
     
          def each hIn
            i = 0
            hIn.key?(:limit) ? limit = hIn._limit : limit = 2
            begin
              MDBX::Database.open(localStorage(hIn._context)._path, localStorage._opts) do |session|
                session.collection(hIn._db)                                
                session.snapshot do
                  session.each do |k, v|
                    myKey = keySalt(k)
                    if (myKey._prefix == hIn._prefix) && i <= limit
                      yield([myKey._key, O.k(v)]) 
                    end
                  end
                end
                session.close
              end
            rescue => e
              Log::App.error(false, "LocalStorage each ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end   
          end
          
          def getAll hIn
            i = 0
            res = O.k
            hIn.key?(:limit) ? limit = hIn._limit : limit = 2
            begin
              MDBX::Database.open(localStorage(hIn._context)._path, localStorage._opts) do |session|
                session.collection(hIn._db)                                
                session.snapshot do |call|
                  call.each do |k, v|
                    myKey = keySalt(k)
                    if (myKey._prefix == hIn._prefix) && i <= limit
                      case v.class.to_s
                      when 'Array', 'Hash'
                        v = O.k(v)
                      end                       
                      #
                      res[myKey._key] = v
                    end
                  end
                end
              end
            rescue => e
              Log::App.error(false, "LocalStorage each ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end
            #
            res
          end      
          
          def getAllKeys hIn
            i = 0
            res = []
            hIn.key?(:limit) ? limit = hIn._limit : limit = 2
            begin
              MDBX::Database.open(localStorage(hIn._context)._path, localStorage._opts) do |session|
                session.collection(hIn._db)                                
                session.snapshot do |call|
                  call.each do |k, v|
                    myKey = keySalt(k)
                    res << myKey._key if (myKey._prefix == hIn._prefix) && i <= limit                     
                  end
                end
              end
            rescue => e
              Log::App.error(false, "LocalStorage each ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end
            #
            res
          end   
           
              
        end
      #end
    end
  end
end
