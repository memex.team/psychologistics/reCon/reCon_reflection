# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Db   
    module Mdbx
      #class Localstore
        class << self
    

          def incrementPlus1 hIn
            get = pull(hIn)
            hIn._value = get.to_i + 1
            push(hIn)
          end
          
          def increment hIn
            get = pull(hIn)
            hIn._value = get.to_i + hIn._inc.to_i
            push(hIn)
          end   
          
              
        end
      #end
    end
  end
end
