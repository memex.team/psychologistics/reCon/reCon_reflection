# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Db   
    module Mdbx
      #class Localstore
        class << self
                
          def pull hIn
            res = false
            #De.bug('pull', hIn)
            begin
              MDBX::Database.open(localStorage(hIn[:ns][:context])._path, localStorage._opts) do |session|
                #p hIn[:ns][:context]
                session.collection(hIn[:ns][:db])
                session.snapshot
                res = pullDataHandler(session, hIn)
                session.rollback
              end
              #
            rescue => e
              Log::App.error(false, "LocalStorage Pull ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end 
            #
            res
          end 
          
          
          def pullContext hIn
            #De.bug(false, hIn)
            begin
              session.collection(hIn[:ns][:db])
              hIn[:db].snapshot do |call|
                return pullDataHandler(call, hIn)
              end
              #
            rescue => e
              Log::App.error(false, "LocalStorage Pull ERROR: #{e}")
              Log::App.error(false, "Backtrace: #{e.backtrace}")
              res = false
            end 
            #
            res
          end    
          

          def keySalt(k)
            res = O.k
            kS = k.split('::')
            res._mod = kS[0]
            res._key = kS[1]
            res._prefix = res._mod
            res
          end           
            
          def pullDataHandler session, hIn           
            res = session["#{hIn[:ns][:prefix]}::#{hIn[:key]}"]
            #
            case res.class.to_s
            when 'Array', 'Hash'
              res = O.k(res)
            end  
            #
            #p res
            res 
          end
                             
              
        end
      #end
    end
  end
end
