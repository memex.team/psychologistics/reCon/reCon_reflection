# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex::Reflection
  module Db   
    module Redis
      #class Localstore
        class << self
          
          def rConnect(type)
          @redis = ::Redis.new(url: "redis://#{ENV['redisHostPort']}/#{ENV["redisDB#{type.to_s}Data"]}",
                              reconnect_attempts: 3600)
                              # driver: :hiredis)  
          @redis
          end
          
          def rAsyncConnect(type)
            uri = URI("redis://#{ENV['redisHostPort']}")
            endpoint = ::Async::IO::Endpoint.tcp(uri.hostname, uri.port)
            @redisAsync = ::Async::Redis::Client.new(endpoint)
            @redisAsync
          end          
          
          def context hIn
            res = O.k  
            res._context = hIn._context
            #p hIn
            res._space   = hIn._space.to_s
            res._type = hIn._type.to_s
            #
            res._db = "#{res._space}:#{res._type}"
            hIn._mod.to_s == "#<Ok>" ?  res._mod = "main" : res._mod = hIn._mod.to_s.strip
            #res._mod[-1, 1].to_i > 0 ? res._mod = res._mod[0..-2]: true
            res._prefix = res._mod
            #
            res
          end
          
          
          def push hIn
            field = "#{hIn[:ns][:context]}:#{hIn[:ns][:db]}:#{hIn[:ns][:mod]}"
            #@redis.multi do |transaction|
            begin
              case hIn[:value].class.to_s
              when 'Integer', 'String'
                v = hIn[:value]
              else
                v = Oj.dump(hIn[:value].to_h)
              end
              res = hIn[:r].hset(field, hIn[:key], v)            
            rescue => e
              p e
              p hIn[:value]
            end
            
            if res == 0
              newDigest = Digest::SHA1.hexdigest(v)
              old = hIn[:r].hget(field, hIn[:key])
              oldDigest = Digest::SHA1.hexdigest(old)
              unless newDigest == oldDigest
                p "## DATA ERROR: key #{hIn[:key]}} misconfigured, redis push for the new data failed"
                p "NEW: #{v}"
                p "OLD: #{old}"
              end
            end            
            res
          end
          
          def key2? hIn
            field = "#{hIn[:ns][:context]}:#{hIn[:ns][:db]}:#{hIn[:ns][:mod]}"
            #@redis.multi do |transaction|
            r = hIn[:r].exists(field, hIn[:key])
            p r
            r >= 1 ? res = true : res = false
            #end    
            res
          end            
          
          def get hIn
            field = "#{hIn[:ns][:context]}:#{hIn[:ns][:db]}:#{hIn[:ns][:mod]}"
            #@redis.multi do |transaction|
            r = hIn[:r].hget(field, hIn[:key])
            r == nil ? res = r : res = Oj.load(r)
            #
            case res.class.to_s
            when 'Integer', 'String', 'NilClass', 'Array', 'TrueClass'
              true
            else
              res = O.k(res)
            end
            #end    
            res
          end   
          alias pull get
          alias key? get
          
          def getAllKeys hIn
            field = "#{hIn[:ns][:context]}:#{hIn[:ns][:db]}:#{hIn[:ns][:mod]}"
            #@redis.multi do |transaction|
            res = hIn[:r].hkeys(field)
            #end            
            res
          end         
          
          def getAll hIn
            field = "#{hIn[:ns][:context]}:#{hIn[:ns][:db]}:#{hIn[:ns][:mod]}"
            #p field
            #@redis.multi do |transaction|
            res = hIn[:r].hgetall(field)
            #end            
            res
          end 
          
          def dropNs hIn
            field = "#{hIn[:ns][:context]}:#{hIn[:ns][:db]}:#{hIn[:ns][:mod]}"
            #@redis.multi do |transaction|
            res = hIn[:r].del(field)
            #end            
            res
          end           
         
              
        end
      #end
    end
  end
end

def store
  ::Re::Db::Redis
end


