# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
if T5_CORE.include?('rest')
  module Memex::Reflection
    module API
      module Base
        class Root < Grape::API
          content_type :json, 'application/json'
          content_type :html, 'text/html'
          format :json

          rescue_from :all do |e|
            res = GrapeErrorHandler(e, :response)
            error!(res, 500)
          end
          # use ElasticAPM::Middleware

          # Auto
          containersResolve = Env::Base.containersResolve
          #Log::App.debug(false, containersResolve)
          containersResolve.each do |n1, n1Params|
            n1Params.each do |n2, n2Params|
              next unless n2 =~ /->/
              n2Params.each do |n3, _n3Params|
                className = "Memex::Containers::#{ENV['memexContainer'].capitalizeFirst}::#{n2.split('->')[1].capitalizeFirst}::#{n3.capitalizeFirst}"
                apiPath = "#{ENV['memexContainer']}/#{n3.downcase}"
                Log::App.info('API path', apiPath)
                # Log::App.info(false, className)
                #
                mount(className.constantize => apiPath)
              end
            end
          end

          add_swagger_documentation format: :json,
                                    info: "", #::Env.apiDesc,
                                    mount_path: "#{ENV['memexContainer']}/oapi",
                                    schemes: %w[http https]
        end
      end
    end
  end
end
