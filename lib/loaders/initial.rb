# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Memex; 
  module Reflection; end 
  module Containers; end
end
  

module Re
  include Memex::Reflection
end

module Con
  include Memex::Containers
end

def generateContainersModules
  FileUtils.rm_rf("../_run/containers/runtime/#{ENV['memexContainer'].downcase}")
  #
  containersResolve = Env::Base.containersResolve[:class]
  # Log::App.debug(false, containersResolve)
  containersResolve.each do |n1, n1Params|
    Object.class_eval(<<-CUT, __FILE__, __LINE__ + 1)
        module Env
          module #{n1.capitalizeFirst}
          end
        end
        module Config
          module #{n1.capitalizeFirst}
          end
        end        
    CUT

    makeRunnableEnvConf(O.k(n1: n1))
  end
    #
  containersResolve.each do |n1, n1Params|  
      Object.class_eval(<<-CUT, __FILE__, __LINE__ + 1)
        module Memex::Containers
          module #{ENV['memexContainer'].capitalizeFirst}
            module #{n1.capitalizeFirst}
            end
          end
        end
    CUT
    #end
  end
  Env::Base.containersResolve[:path].each do |n1, n1Params|
    n1Params.each do |n2, n2Params|        
      makeRunnable(O.k(n1: n1, n2: n2))
    end
  end
end

def generateReModules
  reResolve = Env::Base.reResolve
  reResolve.each do |n1, n1Params|
    n1Params.each do |n1, _n1Params|
      Object.class_eval(<<-CUT, __FILE__, __LINE__ + 1)
        module Memex::Reflection
          module #{n1.capitalizeFirst}
            module #{n1.capitalizeFirst}
            end
          end
        end
      CUT
    end
  end
end

def includeT5
  Dir.glob(File.join("../_run/containers/runtime/#{ENV['memexContainer'].downcase}/**", '*.rb')).each { |f| require(f) }
end

def makeRunnable hIn
  pathSrc = "../containers/#{ENV['memexContainer'].downcase}/#{hIn._n1}/#{hIn._n2}"
  pathDst = "../_run/containers/runtime/#{ENV['memexContainer'].downcase}/#{hIn._n1}/#{hIn._n2}"
  #
  FileUtils.mkdir_p pathDst
  Dir.glob(File.join("#{pathSrc}/**", '*.rb')).each do |file|
    skip = false
    filename = file.split('/').last
    FileUtils.cp "#{pathSrc}/#{filename}", pathDst
    #
    reConify = File.open("#{pathDst}/#{filename}").readlines
    hIn._n1[0..4] =~ /->/ ? mod = hIn._n1.split('->')[1] : mod = hIn._n1
    mod[0..0] =~ /[\d]/  ? mod = mod.split('_')[1] : true
    #
    firstline = "# frozen_string_literal: true\n"
    reConify.shift
    reConify.shift
    reConify.insert(0, firstline)
    #    
    header = "module Memex::Containers; module #{ENV['memexContainer'].capitalizeFirst}; module #{mod.capitalizeFirst};"
    if T5_CORE.include?('rest') && hIn._n1 =~ /->/
      header = header + " class Local;def self.context;Module.nesting;end;end; class #{hIn._n2.capitalizeFirst} < Grape::API\n" 
    elsif hIn._n1 =~ /->/
      header = ''
      skip = true
    else 
      header = header + " class #{hIn._n2.capitalizeFirst}; class << self; def context;Module.nesting;end\n"
    end
    reConify.insert(1, header)
    if T5_CORE.include?('rest') && hIn._n1 =~ /->/
      footer = "end;end;end;end"
    elsif hIn._n1 =~ /->/
      footer = ''
    else 
      footer = "end;end;end;end;end"
    end
    reConify.insert(-1, footer)
    #
    if skip == true
      File.delete("#{pathSrc}/#{filename}")
    else
      File.write("#{pathDst}/#{filename}", reConify.join, mode: "w") 
    end
  end
end

def makeRunnableEnvConf hIn
  pathSrc = "../containers/#{ENV['memexContainer'].downcase}/"
  pathDst = "../_run/containers/runtime/#{ENV['memexContainer'].downcase}/"
  FileUtils.mkdir_p pathDst
  #
  filenames = []
  filenames << 'env.rb' if File.file?("#{pathSrc}/env.rb")
  filenames << 'config.rb' if File.file?("#{pathSrc}/config.rb")
  #
  filenames.each do |filename|
    FileUtils.cp "#{pathSrc}/#{filename}", pathDst      
  #
    reConify = File.open("#{pathDst}/#{filename}").readlines
    #
    firstline = "# frozen_string_literal: true\n"
    reConify.insert(0, firstline)
    #
    header = "module #{filename.split('.')[0].capitalizeFirst}; class << self\ndef context;Module.nesting;end\n"
    reConify.insert(1, header)
    footer = "end;end"
    reConify.insert(-1, footer)
    File.write("#{pathDst}/#{filename}", reConify.join, mode: "w")
  end
end
