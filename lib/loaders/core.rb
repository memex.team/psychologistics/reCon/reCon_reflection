# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

RACK_ENV = (ENV['RACK_ENV'] || 'development').to_sym

require './lib/sys/storables/ok'
class O < Ok; end
require './lib/sys/addons/uuid'
def appbasePath
  "#{File.dirname(File.expand_path(__FILE__))}/../.."
end

def basePathString
  appBase = File.dirname(File.expand_path(__FILE__)).to_s.split('/')
  appBase.pop
  appBase.pop
  appBase.pop
  appBase.join('/')
end

def corePathString
  appBase = File.dirname(File.expand_path(__FILE__)).to_s.split('/')
  appBase.pop
  appBase.pop
  appBase.join('/')
end

def t5Path
  "#{basePathString}/containers/#{ENV['memexContainer']}"
end


def localStorage(context=:nocontext)
  res = O.k
  res._path = "../_run/containers/data/#{context}"
  res._opts = { max_size: 1000000000, max_collections: 30, max_readers: 30 } #, exclusive: true } #no_threadlocal: true }
  res
end    
FileUtils.mkdir_p localStorage(:log)._path

def myCon(hIn)
  res = O.k
  a = hIn[1].to_s.split('::')
  a[3] ? res._type = a[0].downcase  : true
  a[3] ? res._n1   = a[1].downcase : true
  a[3] ? res._stage   = a[2].downcase : res._stage   = a[1].downcase
  a[3] ? res._element = a[3].downcase : res._element = a[2].downcase
  res._env = "::Env".constantize
  res._conf =  "::Config".constantize
  res
end

def metaObject(postfix, context)
  c = myCon(context)
  res = Object.const_get("Memex::Containers::#{c._stage.capitalizeFirst}::#{c._element.capitalizeFirst}::#{postfix}")
  res
end
#
# Bundler.require

def GrapeErrorHandler(e, output)
  trace = []
  i = 0
  e.backtrace.each do |tracePath|
    trace << "APPPATH#{tracePath.delete_prefix!(basePathString)}" if i < 34
    i += 1
  end

  begin
    eName = e.name
  rescue StandardError
    eName = ''
  end
  err = { problemIn: eName, errorMessage: e.message, 'backtrace (last 14)': trace }

  res = false
  case output
  when :response
    res = err
  else
    Log::App.error(false, err)
  end

  res
end

module Env
  module Base
    class << self
      def containersResolve
        res = {path: {}, class: {}}
        Dir.glob(File.join("../containers/#{ENV['memexContainer']}/*/**", '*.rb')).each do |f|
          f = f.split('/') 
          n1 = f[3]
          n1[0..4] =~ /->/ ? n1Mod = n1.split('->')[1] : n1Mod = n1
          n2 = f[4]
          n3 = f[5]
          res[:path][n1] ||= {}
          res[:class][n1Mod] ||= {}
          unless n3 == nil
            res[:path][n1][n2] ||= []
            res[:path][n1][n2].push(n3.split('.')[0])
            #
            res[:class][n1Mod][n2] ||= []            
            res[:class][n1Mod][n2].push(n3.split('.')[0])
          end
        end
        #        
        res
      end

      def reResolve
        res = {}
        Dir.glob(File.join('./lib/re/*/*', '*.rb')).each do |f|
          f = f.split('/')
          n0 = f[3]
          n1 = f[4]
          n2 = f[5]
          res[n0] ||= {}
          res[n0][n1] ||= []
          res[n0][n1].push(n2.split('.')[0])
        end
        res
      end
    end
  end
end
