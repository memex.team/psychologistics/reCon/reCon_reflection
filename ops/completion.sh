_t5_comp()
{
    local cur
	_init_completion || return
	#
    if [[ ${COMP_CWORD} == 1 ]] ; then 
		domain=`find $T5_PATH -mindepth 1 -maxdepth 1 -type d -not \( -path ${T5_PATH}/_* -prune \)|cut -d '/' -f 8`
		COMPREPLY=( $( compgen -W "$domain" -- "$cur" ) )
    fi	

    if [[ ${COMP_CWORD} == 2 ]] ; then 
		container=`find "$T5_PATH/${COMP_WORDS[1]}/containers" -mindepth 1 -maxdepth 1 -type d|cut -d '/' -f 10`
		COMPREPLY=( $( compgen -W "$container" -- "$cur" ) )
    fi	

    return 0
}
complete -F _t5_comp t5
complete -F _t5_comp t5Update
complete -F _t5_comp t5Up
complete -F _t5_comp t5Down
