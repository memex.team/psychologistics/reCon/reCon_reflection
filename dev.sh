#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######

set -e 
inotifywait -m --exclude './_run/*|./6_qc/gauge/*|^\./run/db/*|./*.kate-swp|./\.git|./*.bpmn|.bundle/config' -r -e close_write --format '%w%f' ./ |\
(
while read line; do
  echo $line
  base=`echo $line|cut -d '/' -f 2`  
  container=`echo $line|cut -d '/' -f 3`  
  ! kill `cat /tmp/${container}_log.pid` &> /dev/null
  if [ $base == 'reflections' ] || [ $container == $2 ] ; then
    clear && systemctl --user --no-block restart t5-$1@$2
    echo "$1 - $2 Restarted at $(date +%s) by $line"
    sleep 1
    journalctl -f -o cat --user-unit t5-$1@$2&
    echo $! > /tmp/${container}_log.pid
  fi  
done
)  
